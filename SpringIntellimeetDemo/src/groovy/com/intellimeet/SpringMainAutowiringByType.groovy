package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

/**
 * Created by richa on 23/7/15.
 */
class SpringMainAutowiringByType {

    public static void main(String []args) {

        ApplicationContext beanFactory = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-AutowiringByType.xml')
        User user = beanFactory.getBean('user')
        println(user.properties)
        println(user.vehicle.properties)

    }
}
