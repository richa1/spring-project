package com.intellimeet

import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean


class LifeCycleObjectWithInterface implements InitializingBean , DisposableBean{
    @Override
    void destroy() throws Exception {
        println("destroy method called")

    }

    @Override
    void afterPropertiesSet() throws Exception {
        println("after properties set method called")
    }
}
