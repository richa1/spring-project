package com.intellimeet

import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.BeanPostProcessor


class CustomBeanPostProcessor implements BeanPostProcessor{
    @Override
    Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        println("Bean BeforeInitilalised:" + beanName)
        return bean
    }

    @Override
    Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        println("Bean afterInitialization:" + beanName)
        return bean
    }
}
