package com.intellimeet


class Test {

    int height;
    int width
    String value

    int getHeight() {
        return height
    }

    void setHeight(int height) {
        this.height = height
    }

    int getWidth() {
        return width
    }

    void setWidth(int width) {
        this.width = width
    }

    String getValue() {
        return value
    }

    void setValue(String value) {
        this.value = value
    }

    Test(int height){
        this.height = height
    }

    Test(int width , int height){
        this.width = width
        this.height = height

    }

    Test(int height , String value){
        this.height = height
        this.value = value
    }
}

