package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext


class SpringMainConstructorInjectionDemo {

    public static void main(String []args) {

        ApplicationContext beanFactory = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-constructorInjection.xml')
        Test test1 = beanFactory.getBean('test1')
        println(test1.properties)

        Test test2 = beanFactory.getBean('test2')
        println(test2.properties)


    }
}
