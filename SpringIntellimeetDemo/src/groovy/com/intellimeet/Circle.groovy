package com.intellimeet


class Circle implements Shape {

    Point center

    Point getCenter() {
        return center
    }

    void setCenter(Point center) {
        this.center = center
    }

    void drawShape(){
        println("Drawing circle")
    }
}
