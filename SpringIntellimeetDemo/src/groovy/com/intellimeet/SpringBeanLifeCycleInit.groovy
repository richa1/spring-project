package com.intellimeet

import org.springframework.context.support.AbstractApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

/**
 * Created by richa on 25/7/15.
 */
class SpringBeanLifeCycleInit {

    public static void main(String []args) {

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-lifecycle-init.xml')
        ctx.registerShutdownHook()

    }
}
