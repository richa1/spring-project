package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext


class SpringMainCollectionDemo {

    public static void main(String []args) {

        ApplicationContext beanFactory = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-collection.xml')
        Triangle triangle = beanFactory.getBean('triangle')
        println(triangle.pointList)
        triangle.pointList.each{
            println it.properties
        }
        triangle.pointMap.each{ key, value ->
            println( key + "::" +value.properties)
        }


    }
}
