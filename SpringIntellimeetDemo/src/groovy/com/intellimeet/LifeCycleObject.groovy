package com.intellimeet

import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean

/**
 * Created by richa on 23/7/15.
 */
class LifeCycleObject implements InitializingBean , DisposableBean {



    int var1
    int var2

    int getVar1() {
        return var1
    }

    void setVar1(int var1) {
        println("setting var 1")
        this.var1 = var1
    }

    int getVar2() {
        return var2
    }

    void setVar2(int var2) {
        this.var2 = var2
    }

    LifeCycleObject(int var2){
        println("inside construcor")
        this.var2 = var2
    }
    @Override
    void destroy() throws Exception {
        println("inside destroy")

    }

    @Override
    void afterPropertiesSet() throws Exception {
        println("inside after properties set")
    }

    void init(){
        println("init methodd")
    }
    void cleanUp(){
        println("clean methodd")
    }
}
