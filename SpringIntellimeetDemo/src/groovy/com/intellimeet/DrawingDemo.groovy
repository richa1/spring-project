package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext


class DrawingDemo {

    public static void main(String[] str){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-DrawingDemo.xml')
        Drawing drawing = applicationContext.getBean('drawing')
        drawing.shapeList.each {
            it.drawShape()
        }
    }
}
