package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.AbstractApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext


class SpringMainLifeCycle {

    public static void main(String []args) {

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-lifecycle.xml')
       // LifeCycleObject lifeCycleObject = ctx.getBean('lifecycle')
        ctx.registerShutdownHook()

    }
}
