package com.intellimeet

import org.springframework.context.support.AbstractApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext


class SpringMainLifeCycleUsingInterface {

    public static void main(String []args) {

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-lifecycle-interface.xml')
        ctx.registerShutdownHook()

    }
}
