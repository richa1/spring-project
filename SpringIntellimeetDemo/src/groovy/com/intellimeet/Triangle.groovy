package com.intellimeet

import groovy.transform.ToString;

@ToString
public class Triangle implements Shape{

    int height
    int altitude
    Point point1
    Point point2
    Point point3
    String type

    int getAltitude() {
        return altitude
    }

    void setAltitude(int b) {
        println("setting altitude")
        this.altitude = b
    }

    int getHeight() {
        return height
    }

    void setHeight(int a) {
        println("setting Height")
        this.height = a
    }

    Triangle(int a , int b){
        println("inside the constructor")
        this.height = a
        this.altitude =b
    }


    Point getPoint1() {
        return point1
    }

    void setPoint1(Point point1) {
        this.point1 = point1
    }

    Point getPoint2() {
        return point2
    }

    void setPoint2(Point point2) {
        this.point2 = point2
    }

    Point getPoint3() {
        return point3
    }

    void setPoint3(Point point3) {
        this.point3 = point3
    }


    Triangle(){

    }

    List<Point> getPointList() {
        return pointList
    }

    void setPointList(List<Point> pointList) {
        println("setting point list")
        this.pointList = pointList
    }
    List<Point> pointList

    Map<String , Point> pointMap

    void drawShape(){
        println("Drawing triangle")
    }

}


