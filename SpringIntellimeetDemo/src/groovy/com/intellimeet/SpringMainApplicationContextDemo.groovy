package com.intellimeet

import org.springframework.beans.factory.BeanFactory
import org.springframework.beans.factory.xml.XmlBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext
import org.springframework.core.io.FileSystemResource


class SpringMainApplicationContextDemo {

    public static void main(String []args) {

        ApplicationContext beanFactory = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext.xml')
        Triangle triangle = beanFactory.getBean('triangle')
        println(triangle)
        println(triangle.properties)
        println(triangle.point1)
        println(triangle.point2)
        println(triangle.point3)


    }
}
