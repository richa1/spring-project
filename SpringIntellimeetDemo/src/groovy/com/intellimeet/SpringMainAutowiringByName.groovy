package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext


class SpringMainAutowiringByName {


    public static void main(String []args) {

        ApplicationContext beanFactory = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext-AutowiringByName.xml')
        User user = beanFactory.getBean('user')
        println(user.properties)
        println(user.vehicle.properties)

    }
}
