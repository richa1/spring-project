package com.intellimeet

import org.springframework.beans.factory.BeanFactory
import org.springframework.beans.factory.xml.XmlBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext
import org.springframework.core.io.FileSystemResource


class SpringMainBeanFactoryDemo {


    public static void main(String []args) {

        BeanFactory beanFactory = new XmlBeanFactory(new FileSystemResource('grails-app/conf/spring/applicationContext.xml'))
        Triangle triangle = beanFactory.getBean('triangle')
        println(triangle)

    }
}
