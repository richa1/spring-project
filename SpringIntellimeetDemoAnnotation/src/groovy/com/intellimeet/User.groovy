package com.intellimeet

import org.springframework.stereotype.Component

import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/**
 * Created by richa on 25/7/15.
 */

class User {

    @PostConstruct
    void display(){
        println("post-construct")
    }

    @PreDestroy
    void destroy(){
        println("destroy")
    }
    void initMethod(){
        println("init-method")
    }
}
