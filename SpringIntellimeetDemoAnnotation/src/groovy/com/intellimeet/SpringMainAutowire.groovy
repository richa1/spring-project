package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.AbstractApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

/**
 * Created by richa on 23/7/15.
 */
class SpringMainAutowire {

    public static void main(String []args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContext.xml')
        Triangle triangle = ctx.getBean("triangle")
        println(triangle)
        println(triangle.point.properties)
    }
}
