package com.intellimeet

import org.springframework.stereotype.Component

/**
 * Created by richa on 23/7/15.
 */

class Point {
    int getX() {
        return x
    }

    void setX(int x) {
        this.x = x
    }

    int getY() {
        return y
    }

    void setY(int y) {
        this.y = y
    }

    int getZ() {
        return z
    }

    void setZ(int z) {
        this.z = z
    }
    int x
    int y
    int z

}
