package com.intellimeet

import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

/**
 * Created by richa on 25/7/15.
 */
class SpringMainPostConstruct {

    public static void main(String []args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext('file:grails-app/conf/spring/applicationContextPostConstruct.xml')
        User user = ctx.getBean("user")

    }
}
