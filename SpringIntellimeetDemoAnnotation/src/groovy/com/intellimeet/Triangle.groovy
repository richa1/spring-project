package com.intellimeet

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

import javax.annotation.Resource

/**
 * Created by richa on 23/7/15.
 */

class Triangle {

    @Autowired
    Point point

    Point getPoint() {
        return point
    }

    void setPoint(Point point) {
        println("Setting point")
        this.point = point
    }



}
